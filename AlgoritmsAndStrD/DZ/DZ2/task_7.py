"""
7.	Напишите программу, доказывающую или проверяющую, что для множества
натуральных чисел выполняется равенство: 1+2+...+n = n(n+1)/2,
 где n - любое натуральное число.

 Решите через рекурсию. Решение через цикл не принимается.
Для оценки Отлично в этом блоке необходимо выполнить 5 заданий из 7
"""


def recur_method(numb, right, sum=0):
    if sum == right:
        print(f"Равенство: {sum == right}")

    elif sum < right:
        sum += numb
        return recur_method(numb - 1, right, sum)


try:
    NUMB = int(input("Введите число: "))
    recur_method(NUMB, NUMB * (NUMB + 1) // 2)
except ValueError:
    print("Вы вместо числа ввели строку")
