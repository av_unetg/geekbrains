"""
Задание 1.

Приведен код, который позволяет сохранить в
массиве индексы четных элементов другого массива

Сделайте замеры времени выполнения кода с помощью модуля timeit

Попробуйте оптимизировать код, чтобы снизить время выполнения
Проведите повторные замеры

Добавьте аналитику: что вы сделали и почему
"""

import timeit

"""
2. Во втором массиве сохранить индексы четных элементов первого массива.
Например, если дан массив со значениями 8, 3, 15, 6, 4, 2, то второй массив
надо заполнить значениями 1, 4, 5, 6
(или 0, 3, 4, 5 - если индексация начинается с нуля),
т.к. именно в этих позициях первого массива стоят четные числа.
"""


def func_1(nums):
    """O(n) - линейная сложность"""
    new_arr = []
    for i in range(len(nums)):
        if nums[i] % 2 == 0:
            new_arr.append(i)
    return new_arr


def func_2(nums):
    """O(n) - линейная сложность"""
    return [i for i, el in enumerate(nums) if el % 2 == 0]


# 100
print('--100--')

NUMS = [el for el in range(100)]

print(
    timeit.timeit(
        "func_1(NUMS)",
        setup="from __main__ import func_1, NUMS",
        number=1000))

print(
    timeit.timeit(
        "func_2(NUMS)",
        setup="from __main__ import func_2, NUMS",
        number=1000))

print()

# 1000
print('--1000--')

NUMS = [el for el in range(1000)]

print(
    timeit.timeit(
        "func_1(NUMS)",
        setup="from __main__ import func_1, NUMS",
        number=1000))

print(
    timeit.timeit(
        "func_2(NUMS)",
        setup="from __main__ import func_2, NUMS",
        number=1000))

print()
print('Генераторные выражения выполняются быстрее, чем аналогичный код с циклом for')
