import timeit
import cProfile

def get_count(items):
   return len(items)

def get_sum(items):
   sum_ = 0
   for i in items:
       sum_ += i
   return sum_

def main():
   a = [3,5,6,7]
   s = get_count(a)
   t = get_sum(a)

cProfile.run('main()')

'''
             {timeit.timeit(stmt=func_name + f'(1000)', setup=f'from __main__ import {func_name}, {num_time}, number = 10000'")


  else:
      if id % 2 == 0:
          print(())
          '''
