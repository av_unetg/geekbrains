"""
Задание 2.
Предложить еще какие-гибудь варианты (механизмы, библиотеки) оптимизации и
доказать (наглядно, кодом) их эффективность
"""

from functools import reduce
from memory_profiler import profile


@profile
def function_2(max_value):
    '''Функция возвращает сумму квадратов четных чисел от 1 до max_value'''
    gen = (x ** 2 for x in range(1, max_value) if x % 2 == 0)
    print(type(gen))
    value = reduce(lambda x, y: x + y, gen)
    return value


print(function_2(999999))

'''
Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    10     18.9 MiB     18.9 MiB           1   @profile
    11                                         def function_2(max_value):
    12                                                Функция возвращает сумму квадратов четных чисел от 1 до max_value
    13     19.0 MiB -53944.3 MiB     1500000       gen = (x ** 2 for x in range(1, max_value) if x % 2 == 0)
    14     18.9 MiB      0.0 MiB           1       print(type(gen))
    15     19.0 MiB -35962.9 MiB      999997       value = reduce(lambda x, y: x + y, gen)
    16     18.9 MiB     -0.1 MiB           1       return value
    
    
Уход от массива практически полностью убрал прирост занимаемой памяти
    
Версия Интерпретатора 3.7
Windows 10 64bit
Corei3
'''
