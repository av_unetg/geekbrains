"""
Задание 3 *.
Сделать профилировку для скриптов с рекурсией и сделать,
можно так профилировать и есть ли 'подводные камни'
"""

from functools import reduce
from memory_profiler import profile


@profile
def function_3(max_value):
    '''Функция возвращает сумму квадратов четных чисел от 0 до max_value'''
    gen = [x ** 2 for x in range(1, max_value) if x % 2 == 0]
    value = reduce(lambda x, y: x + y, gen)
    del gen
    return value


print(function_3(999999))

"""
Line #    Mem usage    Increment  Occurences   Line Contents
============================================================
    11     18.9 MiB     18.9 MiB           1   @profile
    12                                         def function_3(max_value):
    13                                             '''Функция возвращает сумму квадратов четных чисел от 0 до max_value'''
    14     39.0 MiB   -166.5 MiB     1000001       gen = [x ** 2 for x in range(1, max_value) if x % 2 == 0]
    15     39.0 MiB      0.0 MiB      999997       value = reduce(lambda x, y: x + y, gen)
    16     19.9 MiB    -19.1 MiB           1       del gen
    17     19.9 MiB      0.0 MiB           1       return value
    
Освободили память с помщью del
    
Версия Интерпретатора 3.7
Windows 10 64bit
Corei3
"""
