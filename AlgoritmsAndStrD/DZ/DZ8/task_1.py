"""
Задание 1.
Реализуйте кодирование строки "по Хаффману".
У вас два пути:
1) тема идет тяжело? тогда вы можете, опираясь на пример с урока, сделать свою версию алгоритма
Разрешается и приветствуется изменение имен переменных, выбор других коллекций, различные изменения
и оптимизации.
КОПИПАСТ ПРИМЕРА ПРИНИМАТЬСЯ НЕ БУДЕТ!
2) тема понятна? постарайтесь сделать свою реализацию.
Вы можете реализовать задачу, например, через ООП или предложить иной подход к решению.

ВНИМАНИЕ: примеры заданий будут размещены в последний день сдачи.
Но постарайтесь обойтись без них.
"""


from collections import Counter, deque


def derevo_haffmama(stroka):
    # Подсчет различающихся символов.
    frequency = Counter(stroka)
    # Сортировка частот по возрастанию
    deque_of_el = deque(sorted(frequency.items(), key=lambda item: item[1]))
    if len(deque_of_el) != 1:  # Еесли строка состоит из одного повторяющего символа.
        # Построения дерева
        while len(deque_of_el) > 1:
            # Суммируем два левых элемента в один и присваеваем ему частоту, равную сумме частот этих элементов
            # получаем новый элемент, а старые (два левых) удаляем из очереди
            sum_of_freq = deque_of_el[0][1] + deque_of_el[1][1]
            sum_of_2_el = {0: deque_of_el.popleft()[0],
                           1: deque_of_el.popleft()[0]}

            # Проверяем, можно ли вставить полученный сложением элемент
            for i, frequency in enumerate(deque_of_el):
                if sum_of_freq > frequency[1]:
                    continue
                else:
                    # Вставляем элемент
                    deque_of_el.insert(i, (sum_of_2_el, sum_of_freq))
                    break
            else:
                # Вставляем корневой элемент
                deque_of_el.append((sum_of_2_el, sum_of_freq))
    else:
        # Один повторяющийся символ = 0
        sum_of_freq = deque_of_el[0][1]
        sum_of_2_el = {0: deque_of_el.popleft()[0], 1: None}
        deque_of_el.append((sum_of_2_el, sum_of_freq))

    return deque_of_el[0][0]


dict_of_codes = dict()


# Дерево
def coding(tree, path=''):
    if not isinstance(tree, dict):  # если не словарь, то добавляем
        dict_of_codes[tree] = path
    else:  # идем дальше по рекурсии
        coding(tree[0], path=f'{path}0')
        coding(tree[1], path=f'{path}1')


# исходная строка
stroka = "beep boop beer!"

# Заполняем таблицу
coding(derevo_haffmama(stroka))

for i in stroka:
    print(dict_of_codes[i], end=' ')  # код символа
print()
