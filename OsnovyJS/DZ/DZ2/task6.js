//6. Реализовать функцию с тремя параметрами: function mathOperation(arg1, arg2, operation),
//где arg1, arg2 – значения аргументов, operation – строка с названием операции.
//В зависимости от переданного значения операции выполнить одну из арифметических операций
//(использовать функции из пункта 3) и вернуть полученное значение (использовать switch).

function sum(x, y){
    return x + y
}

let raznost = function sum(x, y){
    return x - y
}

let mul = (x, y) =>{
    return x * y
}

let dev = (x, y) =>{
    if (y != 0){
        return x / y
    } else return "Деление на ноль"
}

function mathOperation(arg1, arg2, operation){
    switch (operation){
        case 'sum':
            return sum(arg1, arg2);
         break;
        case 'raznost':
            return raznost(arg1, arg2);
         break;
        case 'mul':
            return mul(arg1, arg2);
         break;
        case 'dev':
            return dev(arg1, arg2);
         break;
    }
}

console.log(mathOperation(2, 2, "sum"));
console.log(mathOperation(2, 2, "raznost"));
console.log(mathOperation(2, 2, "mul"));
console.log(mathOperation(2, 2, "dev"));