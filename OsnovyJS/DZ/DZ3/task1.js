// 1. С помощью цикла while вывести все простые числа в промежутке от 0 до 100.

let i = 2;
let j = 2;
let flag = true;
console.log('1'); //т.к. 1 тоже простое число

while (i <=100){
    j = 2;
    flag = true;
    while(j < i){
        if (i % j === 0) flag = false;
        j++;
    }
    if (flag) console.log(i);
    i++;
}