//2. С этого урока начинаем работать с функционалом интернет-магазина.
//Предположим, есть сущность корзины. Нужно реализовать функционал подсчета
//стоимости корзины в зависимости от находящихся в ней товаров.
//3. Товары в корзине хранятся в массиве. Задачи:
//a) Организовать такой массив для хранения товаров в корзине;
//b) Организовать функцию countBasketPrice, которая будет считать стоимость корзины.

let Basket = [{name:'Monitor', pictures:'http://1', count: 1, price: 5500},
              {name:'Printer', pictures:'http://1', count: 2, price: 4000},
              {name:'Keyboard', pictures:'http://1', count: 5, price: 500}];

function countBasketPrice(massiv){
    let price_count = 0;
    massiv.forEach(element =>{
        console.log(element);
        let price_element = element.count * element.price;
        console.log('Стоимость позиции', element.name, price_element, 'руб');
        console.log();
        price_count += price_element;
    })
    return price_count;
}

console.log('Стоимость корзины', countBasketPrice(Basket), 'руб');