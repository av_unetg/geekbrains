//Продолжить работу с интернет-магазином:
//В прошлом домашнем задании вы реализовали корзину на базе массивов.
//Какими объектами можно заменить их элементы?
//Реализуйте такие объекты.
//Перенести функционал подсчета корзины на объектно-ориентированную базу.

class Product {
    constructor() {

    this.name = 'Name';
    this.pictures = 'http://noname';
    this.count = 1;
    this.price = 0;
    }
}

class Monitor extends Product {
    diagonal = 17;
}

class Printer extends Product {
    type = 'Laser';
    format = 'A4';
}

Artikul1 = new Monitor();                             // Мониторов на складе
Artikul1.name = 'LG Flatron 771'
Artikul1.pictures = 'http://LG Flatron 771';
Artikul1.count = 100;
Artikul1.price = 5500;

Artikul2 = new Printer();                            // Принтеров на складе
Artikul2.name = 'HP LG2500'
Artikul2.pictures = 'http://HP LG2500';
Artikul2.count = 200;
Artikul2.price = 4000;

Artikul3 = new Product();                            // Клавиатур на складе
Artikul3.name = 'Genius101'
Artikul3.pictures = 'http://Genius101';
Artikul3.count = 300;
Artikul3.price = 500;

let bucket = {};
let Artikul = [];
let price = 0;

function AddToBucket(Artikul, count){
    bucket += Artikul;
    price += Artikul.price*count;
    console.log(Artikul.name + ' : '+Artikul.price+'руб х '+count+'шт = ' +Artikul.price*count+'руб');
    return price;
}

// Пользователь добавляет элементы в корзину
AddToBucket(Artikul1, 1);
AddToBucket(Artikul2, 2);
AddToBucket(Artikul3, 5);

console.log('Итого наименований на '+price+'руб');
