let canvas = document.querySelector('#canvas');
let ctx = canvas.getContext("2d");
let w = canvas.width / 8; //ширина клетки
let h = w; // высота клетки

const drawRect = (x, y, color) => {
    ctx.fillStyle = color;
    ctx.fillRect(x * w, y * h, w, h);
}

const clearBoard = () => {
    const colorDark = '#369';
    const colorLight = '#fff';
    ctx.fillStyle = colorDark;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    for(let i = 0; i < 8; i+=2){
        for(let j = 0; j < 8; j+=2){
            drawRect(i, j, colorLight);
            drawRect(i+1, j+1, colorLight); 
        }    
    }

    const leters = ['a','b','c','d','e','f','j','h'];
    ctx.fillStyle = '#000';
    ctx.font="15px Georgia";
    for(let i = 0; i < 8; i++){
        ctx.fillText(i+1, 490, 35+i*63);
        ctx.fillText(i+1, 5, 35+i*63);
        ctx.fillText(leters[i],25+i*63,495);
        ctx.fillText(leters[i],25+i*63,12);
    }
    
} 

clearBoard();