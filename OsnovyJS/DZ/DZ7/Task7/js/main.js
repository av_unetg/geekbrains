function getCounter() {
    let count = 0;

    return () => count++;
}

const counter = getCounter();


let tasks = [
   new Task('static'),
   new Task('static1'),
   new Task('static2'),
   new Task('static3')
];

let isMove = false;
let movedTask = null;

function moveHandler() {
    if(isMove) {

    }
}

function deleteTaskHandler() {
    this.done = !this.done;
    renderTasks(tasks);
}



function Task(title) {
    this.id = counter();
    this.title = title;
    this.done = false;

    this.deleteHandler = deleteTaskHandler
}

function getTaskElement(task) {
    const containerEl = document.createElement("div");
    const inputEl = document.createElement("input");
    const titleEl = document.createElement("p");
    const buttonEl = document.createElement("button");

    containerEl.classList.add('task-item');
    inputEl.setAttribute('type', 'checkbox');
    if(task.done) {
        inputEl.setAttribute('checked', 'checked');
        containerEl.classList.add('done')
    }
    titleEl.classList.add('task-item__title');
    titleEl.textContent = task.title;
    buttonEl.classList.add('task-item__btn');
    buttonEl.textContent = 'delete';

    inputEl.addEventListener('click', task.deleteHandler.bind(task));
    containerEl.addEventListener('mousedown', () => {
        isMove = true;
        movedTask = task;
        console.log(movedTask)
        tasks = tasks.filter((task) => {
            console.log(movedTask.id, task.id)
            return movedTask.id !== task.id
        });
        renderTasks(tasks);
        console.log('move start')
    });

    containerEl.addEventListener('mouseup', (e) => {
        if(isMove) {
            isMove = false;
            const elAfter = e.target;
            const index = tasks.indexOf(elAfter);
            tasks = [...tasks.slice(0, index), movedTask, ...tasks.slice(index)];
            movedTask = null;
            renderTasks(tasks);
            console.log('move end')
        }
    })

    containerEl.appendChild(inputEl);
    containerEl.appendChild(titleEl);
    containerEl.appendChild(buttonEl);

    return containerEl;
}

function renderTasks(tasks) {
    const taskListEl = document.querySelector('#task-list');
    taskListEl.textContent = '';

    tasks.forEach(task => {
        taskListEl.appendChild(getTaskElement(task));
    }); 

    console.log(tasks)
}

function addTaskHandler() {
    const inputEl = document.querySelector('#new-task');

    if(inputEl.value) {
        tasks.push(new Task(inputEl.value));
        renderTasks(tasks);
    }
}

const addTaskBtnEl = document.querySelector('#add-task-btn');
addTaskBtnEl.addEventListener('click', addTaskHandler);


renderTasks(tasks);

