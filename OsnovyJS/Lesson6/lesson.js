document.querySelector('#like-btn')
    .addEventListener('click', (e) => {
        e.stopPropagation()
        const countEl = document.querySelector('#like-count');
        const count = Number(countEl.textContent);
        countEl.textContent = count + 1;
    })

document.querySelector('#post')
    .addEventListener('click', (e) => {
        alert('добавлен в корзину')
    } )

// setTimeout(
//     () => {
//         console.log('hello')
//     },
//     5000
// )

// function clickHandler(){
//     console.log('handler');
//     //div.removeEventListener('click', clickHandler);
// }

// const div = document.querySelector('div');
// div.addEventListener('click', clickHandler);

// //div.onclick = clickHandler;