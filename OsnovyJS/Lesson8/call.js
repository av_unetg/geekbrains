const Person = {
    name: 'John',
    lastname: 'Doe',

    getFullName(prefix, nikname) {
        console.log(`${prefix}. ${this.name} ${nikname} ${this.lastname}`);
    }
}

const handler = {
    greeting(cb) {
        console.log('Hello');
        cb.call(Person, 'Mr', 'Red')
    }
}

//Person.getFullName('Mr', 'Black');

handler.greeting(Person.getFullName)