# 1. Поработайте с переменными, создайте несколько, выведите на экран,
# запросите у пользователя несколько чисел и строк и сохраните в переменные,
# выведите на экран.

user_name = input('Здравствуйте, как вас зовут?')
perem_a = int(input("задайте целое число а"))
perem_b = int(input("задайте целое число b"))
user_instruction = input('задайте необходимое действие (+, -, *, /, **)')

if user_instruction == '+':
    user_result = perem_a + perem_b
elif user_instruction == '-':
    user_result = perem_a - perem_b
elif user_instruction == '*':
    user_result = perem_a * perem_b
elif user_instruction == '/':
    user_result = perem_a / perem_b
elif user_instruction == '**':
    user_result = perem_a ** perem_b

print(f"{user_name}, {perem_a} {user_instruction} {perem_b} = {user_result}")
