# 3. Узнайте у пользователя число n. Найдите сумму чисел n + nn + nnn.
# Например, пользователь ввёл число 3. Считаем 3 + 33 + 333 = 369.

user_number = input('Введите любое число')

number1 = int(user_number)
number2 = int(user_number*2)
number3 = int(user_number*3)

print(f"{number1} + {number2} + {number3} = {number1+number2+number3}.")