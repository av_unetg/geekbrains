# 4. Пользователь вводит целое положительное число.
# Найдите самую большую цифру в числе. Для решения
# используйте цикл while и арифметические операции.

user_number = int(input('Введите целое положительное число'))

big_number = 0

while user_number >= 1:
    chislo = user_number % 10
    if user_number % 10 == 9:
        big_number = 9
        break
    elif user_number % 10 > big_number:
        big_number = chislo
    user_number //= 10

print(f"Cамая большая цифра в числе: {big_number}")
