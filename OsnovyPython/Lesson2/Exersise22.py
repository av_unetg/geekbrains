# 2. Для списка реализовать обмен значений соседних элементов,
# т.е. Значениями обмениваются элементы с индексами 0 и 1, 2 и 3 и т.д.
# При нечетном количестве элементов последний сохранить на своем месте.
# Для заполнения списка элементов необходимо использовать функцию input().

el_count = int(input('Введите количество элементов списка'))
my_list = []

for i in range(1, el_count + 1):
    my_list.append(i)

print(my_list)
my_list_1 = my_list[1::2]
my_list_2 = my_list[::2]

for el in range(len(my_list_1)):
    my_list[el * 2] = my_list_1[el]
    my_list[el * 2 + 1] = my_list_2[el]

print(my_list)
