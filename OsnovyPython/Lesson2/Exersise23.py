# 3. Пользователь вводит месяц в виде целого числа от 1 до 12.
# Сообщить к какому времени года относится месяц (зима, весна, лето, осень).
# Напишите решения через list и через dict.

mons_number = int(input('Введите номер месяца в году'))
my_lyst = ["Весна", "Лето", "Oсень", "Зима"]
my_dict = {0: 'Весна', 1: 'Лето', 2: 'Осень', 3: 'Зима', }

if mons_number >= 3 and mons_number <= 5:
    mons_number = 0
elif mons_number >= 6 and mons_number <= 8:
    mons_number = 1
elif mons_number >= 9 and mons_number <= 11:
    mons_number = 2
else:
    mons_number = 3

print(f"По списку этот месяц соответствует времени года: {my_lyst[mons_number]}")
print(f"По словарю этот месяц соответствует времени года: {my_dict[mons_number]}")
