# 4. Пользователь вводит строку из нескольких слов, разделённых пробелами.
# Вывести каждое слово с новой строки. Строки необходимо пронумеровать.
# Если в слово длинное, выводить только первые 10 букв в слове.

my_string = input("Введите несколько слов, разделенных пробелами").split(" ")
str = ""
my_count = 1
for el in range(len(my_string)):
    for letter in my_string[el]:
        str += letter
        my_count += 1
        if my_count > 10:
            break
    print(f"{el+1}-{str}")
    str = ""
    my_count = 1
