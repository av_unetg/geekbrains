# 2. Реализовать функцию, принимающую несколько параметров,
# описывающих данные пользователя: имя, фамилия, год рождения,
# город проживания, email, телефон. Функция должна принимать
# параметры как именованные аргументы. Реализовать вывод данных
# о пользователе одной строкой.

def my_user_info(first_name, last_name, year, city, email, phone_number):
    """

    :param first_name: Фамилия
    :param last_name: Имя
    :param year: год Рождения
    :param city: город проживания
    :param email: Адрес электронной почты
    :param phone_number: номер телефона
    :return: Строка и информацией о пользователе
    """
    print('Вот, что мы знаем о пользователе:')
    return f"{first_name} {last_name}, {year} год рождения, город {city}, Email: {email}, Тел: {phone_number}"


print(my_user_info(first_name='Иванов', last_name='Иван', year='1985', city='Москва', email='ivan@ivanov.ru',
                   phone_number='88007776665'))
