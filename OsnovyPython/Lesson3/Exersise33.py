# 3. Реализовать функцию my_func(), которая принимает три
# позиционных аргумента, и возвращает сумму наибольших двух аргументов.

def my_func(arg_1, arg_2, arg_3):
    """

    :param arg_1: первое число
    :param arg_2: второе число
    :param arg_3: третье число
    :return: сумма наибольших двух аргументов
    """
    try:
        rezult = arg_1 + arg_2 + arg_3 - min(arg_1, arg_2, arg_3)

    except TypeError:
        print("Попробуйте еще раз,")
        return print('функция принимает только три числа')

    return f"Сумма наибольших двух аргументов: {rezult}"


print(my_func(10, 20, 30))
