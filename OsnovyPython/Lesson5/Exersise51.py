# 1. Создать программно файл в текстовом формате, записать в него построчно данные,
# вводимые пользователем. Об окончании ввода данных свидетельствует пустая строка.

i = 1
my_list = []
while True:
    my_str = input(F'Введите {i}-ю строку')
    if my_str == '':
        break
    else:
        my_list.append(my_str)
        i += 1

with open("file51.txt", 'w') as f_obj:
    for el in my_list:
        f_obj.writelines(el + '\n')
