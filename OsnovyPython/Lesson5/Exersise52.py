# 2. Создать текстовый файл (не программно), сохранить в нем несколько строк,
# выполнить подсчет количества строк, количества слов в каждой строке.

with open("file52.txt", encoding="utf-8") as f_obj:
    my_lyst = f_obj.readlines()

print(my_lyst)
print(f'всего в файле строк- {len(my_lyst)}')

for i in range(len(my_lyst)):
    print(f'В {i+1}-ой строке слов- {len(my_lyst[i].split())}')
