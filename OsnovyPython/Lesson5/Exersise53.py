# 3. Создать текстовый файл (не программно), построчно записать фамилии сотрудников
# и величину их окладов. Определить, кто из сотрудников имеет оклад менее 20 тыс.,
# вывести фамилии этих сотрудников. Выполнить подсчет средней величины дохода сотрудников.

with open("file53.txt", encoding="utf-8") as f_obj:
    my_lyst = f_obj.readlines()

print(my_lyst)
people_lyst = []
dohod = 0

for i in range(len(my_lyst)):
    dohod += float(my_lyst[i].split()[1])
    if float(my_lyst[i].split()[1]) < 20000:
        people_lyst.append(f'{my_lyst[i].split()[0]} - оклад менее 20тыс')
    print(my_lyst[i], end='')

print()
print()
for el in people_lyst:
    print(el)

print()
print(f"Средний доход сотрудников = {dohod/len(my_lyst):2}")
