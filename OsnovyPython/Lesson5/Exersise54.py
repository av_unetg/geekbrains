# 4. Создать (не программно) текстовый файл со следующим содержимым:
# One — 1
# Two — 2
# Three — 3
# Four — 4
# Необходимо написать программу, открывающую файл на чтение и считывающую
# построчно данные. При этом английские числительные должны заменяться на
# русские. Новый блок строк должен записываться в новый текстовый файл.

my_lyst = 't'
my_dict = {'One': 'Один', 'Two': 'Два', 'Three': 'Три', 'Four': 'Четыре'}
new_lyst = []
new_lyst1 = []

i = 0

with open("file54.txt", encoding="utf-8") as f_obj:
    while my_lyst != '':
        my_lyst = f_obj.readline()
        new_lyst1 = my_lyst.split()
        for el in my_dict:
            if new_lyst1.count(el):
                new_lyst1[0] = my_dict.get(el)
        new_lyst.append(new_lyst1)
        print(my_lyst, end='')

print()

with open("file54_2.txt", 'w', encoding="utf-8") as f_obj:
    for el in new_lyst:
        for i in el:
            f_obj.writelines(i + ' ')
        f_obj.writelines('\n')
