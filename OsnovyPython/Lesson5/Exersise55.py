# 5. Создать (программно) текстовый файл, записать в него программно набор чисел, разделенных
# пробелами. Программа должна подсчитывать сумму чисел в файле и выводить ее на экран.

my_lyst = [el for el in range(10)]
print(my_lyst)

with open("file55.txt", 'w', encoding="utf-8") as f_obj:
    for el in my_lyst:
        f_obj.writelines(str(el) + ' ')

with open("file55.txt", 'r', encoding="utf-8") as f_obj:
    my_lyst = f_obj.read().split()

my_count = 0
for el in my_lyst:
    my_count += int(el)

print(f'Сумма чисел в файле: {my_count}')
