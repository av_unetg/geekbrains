# 7. Создать (не программно) текстовый файл, в котором каждая строка должна содержать
# данные о фирме: название, форма собственности, выручка, издержки.
# Пример строки файла: firm_1 ООО 10000 5000.
# Необходимо построчно прочитать файл, вычислить прибыль каждой компании, а также
# среднюю прибыль. Если фирма получила убытки, в расчет средней прибыли ее не включать.
# Далее реализовать список. Он должен содержать словарь с фирмами и их прибылями,
# а также словарь со средней прибылью. Если фирма получила убытки, также добавить ее в словарь
# (со значением убытков).
# Пример списка: [{“firm_1”: 5000, “firm_2”: 3000, “firm_3”: 1000}, {“average_profit”: 2000}].
# Итоговый список сохранить в виде json-объекта в соответствующий файл.
# Пример json-объекта:
# [{"firm_1": 5000, "firm_2": 3000, "firm_3": 1000}, {"average_profit": 2000}]
#
# Подсказка: использовать менеджеры контекста.

import json

my_lyst = []
my_dict1 = {}
my_dict2 = {}
average_profit = 0
i = 0

with open("file57.txt", encoding="utf-8") as f_obj:
    for line in f_obj:
        print(line, end='')
        number = int(line.split()[2]) - int(line.split()[3])
        my_dict1[line.split()[0]] = number
        if number > 0:
            average_profit += number
            i += 1

my_dict2['average_profit'] = average_profit / i
my_lyst.append(my_dict1)
my_lyst.append(my_dict2)
print()
print(my_lyst)

with open("file57.json", 'w', encoding="utf-8") as write_f:
    json.dump(my_lyst, write_f, ensure_ascii=False, indent=4)
