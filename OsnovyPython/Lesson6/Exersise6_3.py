# 3. Реализовать базовый класс Worker (работник), в котором определить
# атрибуты: name, surname, position (должность), income (доход).
# Последний атрибут должен быть защищенным и ссылаться на словарь, содержащий
# элементы: оклад и премия, например, {"wage": wage, "bonus": bonus}.
# Создать класс Position (должность) на базе класса Worker. В классе Position
# реализовать методы получения полного имени сотрудника (get_full_name) и дохода
# с учетом премии (get_total_income). Проверить работу примера на реальных данных
# (создать экземпляры класса Position, передать данные, проверить значения атрибутов,
# вызвать методы экземпляров).

class Worker:
    def __init__(self, name, surname, position, income):
        self.name = name
        self.surname = surname
        self.position = position
        self._income = income
        self.wage = income.get('wage')
        self.bonus = income.get('bonus')


class Position(Worker):
    pass

    def get_full_name(self):
        return (f'{self.name} {self.surname} - {self.position}')

    def get_total_income(self):
        return (f'имеет доход {self.wage + self.bonus} руб')


a = Position('Иван', 'Иванов', 'Менеджер', {"wage": 30000, "bonus": 20000})

print('Проверка атрибутов:')
print(a.surname)
print(a.name)
print(a.position)
print(a._income)
print()
print('Проверка методов:')
print(a.get_full_name())
print(a.get_total_income())
