# 4. Реализуйте базовый класс Car. У данного класса должны быть следующие атрибуты:
# speed, color, name, is_police (булево). А также методы: go, stop, turn(direction),
# которые должны сообщать, что машина поехала, остановилась, повернула (куда).
# Опишите несколько дочерних классов: TownCar, SportCar, WorkCar, PoliceCar.
# Добавьте в базовый класс метод show_speed, который должен показывать текущую скорость
# автомобиля. Для классов TownCar и WorkCar переопределите метод show_speed.
# При значении скорости свыше 60 (TownCar) и 40 (WorkCar) должно выводиться сообщение
# о превышении скорости. Создайте экземпляры классов, передайте значения атрибутов.
# Выполните доступ к атрибутам, выведите результат. Выполните вызов методов и также покажите результат.

class Car:
    count = 0
    def __init__(self, speed, color, name, is_police):
        self.speed = speed
        self.color = color
        self.name = name
        self.is_police = is_police
        Car.count += 1

    def go(self):
        print('Машина поехала')

    def stop(self):
        print('Машина остановилась')

    def turn(self):
        if Car.count % 2 == 0:
            print('Машина повернула направо')
        else:
            print('Машина повернула налево')

    def show_speed(self):
        print(f'Скорость автомобиля - {self.speed} км/ч')

class TownCar(Car):
    pass

    def show_speed(self):
        if self.speed > 60:
            print(f'Внимание, превышение скорости на {self.speed - 60}км/ч')
        print(f'Скорость городского автомобиля - {self.speed} км/ч')


class SportCar(Car):
    pass

class WorkCar(Car):
    pass

    def show_speed(self):
        if self.speed > 40:
            print(f'Внимание, превышение скорости на {self.speed - 40}км/ч')
        print(f'Скорость рабочего автомобиля - {self.speed} км/ч')


class PoliceCar(Car):
    pass

a = TownCar(80, 'Голубой', 'Газель', False)
print(f'Автомобиль {a.name}, цвет {a.color}, скорость {a.speed}км/ч, работает в полиции - {a.is_police}')
a.go()
a.show_speed()
a.turn()
a.stop()

print()
b = SportCar(100, 'Серый', 'Ламборджини', False)
print(f'Автомобиль {b.name}, цвет {b.color}, скорость {b.speed}км/ч, работает в полиции - {b.is_police}')
b.go()
b.show_speed()
b.turn()
b.stop()

print()
c = WorkCar(40, 'Белый', 'УАЗ', False)
print(f'Автомобиль {c.name}, цвет {c.color}, скорость {c.speed}км/ч, работает в полиции - {c.is_police}')
c.go()
c.show_speed()
c.turn()
c.stop()

print()
d = PoliceCar(70, 'Серебристый', 'Лада-Веста', True)
print(f'Автомобиль {d.name}, цвет {d.color}, скорость {d.speed}км/ч, работает в полиции - {d.is_police}')
d.go()
d.show_speed()
d.turn()
d.stop()
