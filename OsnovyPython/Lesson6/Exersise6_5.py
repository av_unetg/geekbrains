# 5. Реализовать класс Stationery (канцелярская принадлежность).
# Определить в нем атрибут title (название) и метод draw (отрисовка).
# Метод выводит сообщение “Запуск отрисовки.” Создать три дочерних класса
# Pen (ручка), Pencil (карандаш), Handle (маркер). В каждом из классов
# реализовать переопределение метода draw. Для каждого из классов методы
# должен выводить уникальное сообщение. Создать экземпляры классов и проверить,
# что выведет описанный метод для каждого экземпляра.

class Stationery:
    title = 'канцелярская принадлежность'

    def draw(self):
        return 'Запуск отрисовки'


class Pen(Stationery):
    def draw(self):
        return 'Ручка'


class Pencil(Stationery):
    def draw(self):
        return 'Карандаш'


class Handle(Stationery):
    def draw(self):
        return 'Маркер'


a = Stationery()
print(a.draw())
print(a.title)

b = Pen()
print(b.draw())

c = Pencil()
print(c.draw())

d = Handle()
print(d.draw())
