# 1. Реализовать класс Matrix (матрица). Обеспечить перегрузку конструктора класса (метод __init__()),
# который должен принимать данные (список списков) для формирования матрицы.
# Подсказка: матрица — система некоторых математических величин, расположенных в виде прямоугольной схемы.
# Примеры матриц вы найдете в методичке.
# Следующий шаг — реализовать перегрузку метода __str__() для вывода матрицы в привычном виде.
# Далее реализовать перегрузку метода __add__() для реализации операции сложения двух объектов
# класса Matrix (двух матриц). Результатом сложения должна быть новая матрица.
# Подсказка: сложение элементов матриц выполнять поэлементно — первый элемент первой строки
# первой матрицы складываем с первым элементом первой строки второй матрицы и т.д.

class Matrix:
    def __init__(self, matrix_arg):
        self.matrix_arg = matrix_arg

    def __str__(self):  # в разборе ДЗ есть вариант лучше: '\n'.join(map(str, self.matrix_arg))
        matrix_str = ''
        for el in self.matrix_arg:
            for i in range(len(el)):
                matrix_str += str(el[i]) + '\t'
            matrix_str += '\n'
        return matrix_str

    def __add__(self, other):
        new_matrix = []
        for el in range(len(self.matrix_arg)):
            new_matrix.append([])
            for i in range(len(self.matrix_arg[el])):
                new_matrix[el].append(self.matrix_arg[el][i] + other.matrix_arg[el][i])
        return Matrix(new_matrix)


m1 = Matrix([[1, 2, 3], [2, 3, 4], [1, 5, 6]])
m2 = Matrix([[7, 8, 3], [3, 9, 10], [6, 11, 12]])
print(m1)
print(m2)
print(m1 + m2)
